//Terrence Mitchell - tjm0276
//Nelson Alfaro - nsa0084
//Date: 02-22-20
//Major Assignment 1
//CSCE 3600.070
//Description: Disassembles a binary instruction sets and puts it into assembly language 
//This program takes the name of the binary file as an argument

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]){

	FILE *f;
	unsigned int i;

	
	//If the user typed more than 1 argument
	if (argc > 1)
	{
		f = fopen(argv[1], "rb");
		if (f == NULL)
		{
		perror(argv[1]);
	       	exit(1);

		}

	}
	else {
		exit(1);
	}



 	unsigned short  myLine;
	unsigned short *myLinePoint;
	myLinePoint = &myLine;

		
	//Run through file
	while (! feof(f))
		{

		fread(myLinePoint, 2, 1, f);
			
		int shift;
		int regA, regB, regC, regHex, regNum;
		int opCode;

		opCode = myLine >> 13;
		
		//Check the opcode to see which it matches
		if((opCode & 0x7) == 0x0){
			
			printf("ADD  ");

			//Shift and AND it to get register A
			shift = myLine >> 10;
			regA = shift & 0b111;
			printf("R%d, ", regA);

			//Shift and AND it to get register B
			shift = myLine >> 7;
			regB = shift & 0b111;
			printf("R%d, ", regB);

			//AND the whole line to get register C
			regC = myLine & 0b111;
			printf("R%d \n", regC);
	
			}
		else if ((opCode & 0x7) == 0x1){

			printf("ADDI ");

			shift = myLine >> 10;
			regA = shift & 0b111;
			printf("R%d, ", regA);

			shift = myLine >> 7;
			regB = shift & 0b111;
			printf("R%d, ", regB);

			//1111111 is value in binary for the last 7 bits of LUI
			regNum = myLine & 0b1111111;
			printf("%d \n", regNum);
			
			}
		else if((opCode & 0x7) == 0x2){
			
			printf("NAND ");
	
			shift = myLine >> 10;
			regA = shift & 0b111;
			printf("R%d, ", regA);

			shift = myLine >> 7;
			regB = shift & 0b111;
			printf("R%d, ", regB);

			regC = myLine & 0b111;
			printf("R%d \n", regC);
					
			}
		else if((opCode & 0x7) == 0x3){

			printf("LUI  ");

			shift = myLine >> 10;
			regA = shift & 0b111;
			printf("R%d, ", regA);

			//AND the whole line to get Hex
			//1023 is the decimal value in binary for the last 7 bits of LUI
			regHex = myLine & 1023;
			printf("0x%x \n", regHex);

			}
		else if((opCode & 0x7) == 0x4){

			printf("SW   ");

			shift = myLine >> 10;
			regA = shift & 0b111;
			printf("R%d, ", regA);

			
			shift = myLine >> 7;
			regB = shift & 0b111;
			printf("R%d, ", regB);
			
			regNum = myLine & 0b1111111;
			printf("%d \n", regNum);

			}

		else if ((opCode & 0x7) == 0x5){

			printf("LW   ");

			shift = myLine >> 10;
			regA = shift & 0b111;
			printf("R%d, ", regA);

			shift = myLine >> 7;
			regB = shift & 0b111;
			printf("R%d, ", regB);

			regNum = myLine & 0b1111111;
			printf("%d \n", regNum);

			
			}	
		else if ((opCode & 0x7) == 0x6){

			printf("BEQ  ");

			shift = myLine >> 10;
			regA = shift & 0b111;
			printf("R%d, ", regA);

			shift = myLine >> 7;
			regB = shift & 0b111;
			printf("R%d, ", regB);

			regNum = myLine & 0b1111111;
			printf("%d \n", regNum);
			
			}


		else if ((opCode & 0x7) == 0x7){

			printf("JALR ");

			shift = myLine >> 10;
			regA = shift & 0b111;
			printf("R%d, ", regA);

			shift = myLine >> 7;
			regB = shift & 0b111;
			printf("R%d \n", regB);
			
			}

		}//End of while
}//End of main
